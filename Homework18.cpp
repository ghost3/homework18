#include <iostream>
//#include <string>
template <class T>
class Cell
{
public:
    T m_data;                       //data that is cointaining in the Cell
    Cell* m_prev = nullptr;         //pointer to the previous cell
};

template <class T>
class Stack
{
public:
    Stack()
    {

    }

    Cell<T>* m_top = nullptr;                      //pointer to the top Cell of Stack. If there is no Cells it points to the nullptr.



    void m_push(T pushData)
    {
        Cell<T>* newCell = new Cell<T>;         //allocating memory for the new Cell
        newCell->m_data = pushData;             //adding data to the new Cell

        newCell->m_prev = m_top;                //then make the top Cell to be prevoius Cell
        m_top = newCell;                        //and make the new Cell to be the next top Cell
    }

    void m_pop()
    {
        if(m_top != nullptr)                    //check if there are cells exist
        {
            Cell<T>* cellToKill = m_top;        //make a new pointer to the cell that we want to delete
            m_top = m_top->m_prev;              //make a previous Cell to be the top Cell
            delete cellToKill;                  //delete the Cell that we want to delete
        }
        else
            std::cout << "Oops! There are no more cells!\n";
    }

    void m_printTop()
    {
        if(m_top != nullptr)                    //check if there are cells exist
            std::cout << m_top->m_data << '\n'; //print the data of the top cell
        else
            std::cout << "Oops! There is nothing to print!\n";
    }

    void m_printAll()
    {
        for(Cell<T>* current = m_top ; current != nullptr; current = current->m_prev)   //slide down the Cells before Current meets nullptr
            std::cout << current->m_data << '\n';                                       //print the data of the Current cell
    }

    ~Stack()
    {
        while(m_top != nullptr)
            m_pop();
    }

};

int main()
{
    std::cout << "Stack of Ints:\n";
    Stack<int> stackOfInts;

    stackOfInts.m_push(1);
    stackOfInts.m_push(2);
    stackOfInts.m_push(3);
    stackOfInts.m_push(4);
    stackOfInts.m_push(5);
    stackOfInts.m_pop();
    stackOfInts.m_printAll();

    std::cout << "\nStack of Floats:\n";
    Stack<float> stackOfFloats;

    stackOfFloats.m_push(11.1f);
    stackOfFloats.m_push(12.2f);
    stackOfFloats.m_push(13.3f);
    stackOfFloats.m_push(14.4f);
    stackOfFloats.m_push(15.5f);
    stackOfFloats.m_pop();
    stackOfFloats.m_printAll();

    std::cout << "\nStack of Strings:\n";
    Stack<std::string> stackOfStrings;

    stackOfStrings.m_push("One");
    stackOfStrings.m_push("Two");
    stackOfStrings.m_push("Three");
    stackOfStrings.m_push("Four");
    stackOfStrings.m_push("Five");
    stackOfStrings.m_pop();
    stackOfStrings.m_printAll();

 
}

